let body = ''
fetch('https://api.datos.gob.mx/v1/calidadAire').then(response => response.json()).then((Datos) => {
    let resultado = Datos.results
    body = ''
    document.getElementById('data').innerHTML = body
    console.log(resultado)
    resultado.forEach(resultado => {
        resultado.stations.forEach(station => {
            body += `<tr>${Body(station)}</tr>`
        })
    });
    document.getElementById('data').innerHTML = body
});

function Body (station) {
    var arrayIndexes = getIndexes(station.indexes)
    var arrayMeasurements = getMeasurements(station.measurements)
    return `<td>${station.name}</td><td id="borde">${station.id}</td> ${arrayIndexes} ${arrayMeasurements}`
}

function getIndexes (indexes) {
    let data = ''
    indexes.forEach(index => {
        data += `<td>${index.scale}</td><td>${index.value}</td><td id="borde">${index.calculationTime}</td>`
    })
    return data
}

function getMeasurements (measurements) {
    let data = ''
    measurements.forEach(ms => {
        data += `<td>${ms.value}</td><td>${ms.unit}</td><td>${ms.pollumant}</td>`
    })
    return data
}

class Busqueda extends HTMLElement {
    constructor() {
        super()
        this.addEventListener('click', function(e) {
            const localidad = document.getElementById('localidad').value
            if(localidad.length != 0){
                fetch('https://api.datos.gob.mx/v1/calidadAire').then(response => response.json()).then(Datos => {
                    body = ''
                    let resultado = Datos.results
                    let busqueda = []
                    resultado.forEach(s => {
                        s.stations.forEach(n => {
                            if(n.name.indexOf(localidad) !== -1){
                                busqueda.push(s)
                            }
                        })
                    })
                    if(busqueda.length != 0) {
                        resultado = busqueda
                        body = ''
                        document.getElementById('data').innerHTML = body
                        resultado.forEach(resultado => {
                            resultado.stations.forEach(station => {
                                body += `<tr>${Body(station)}</tr>`
                            })
                        })
                        document.getElementById('data').innerHTML = body
                    }else{
                        alert('Verifique la localidad')
                    }
                })
            }else{
                alert('Sin el nombre de la localidad no se puede iniciar bsuqueda')
            }
        })
    }
}

class Reload extends HTMLElement {
    constructor() {
        super()
        this.addEventListener('click', function(e) {
            location.reload()
        })
    }
}

customElements.define('mi-busqueda', Busqueda)
customElements.define('mi-reload', Reload)